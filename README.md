# Rust Error Handling

A brief writeup about error handling in rust

## Error Handling Goals

1. **Automatic recovery** where possible (i.e. retrying a failed request)
2. Provide the **operator** (someone familiar with the inner system workings) with a clear report on what went wrong
3. Provide the **user** (who doesn't know the inner workings of the system) with clear feedback appropriate to their level of understanding

## Rust Result Type
```rust
enum Result<T, E> {
   Ok(T),
   Err(E),
}
```

- Result is a generic enum with two possibilities: Ok, and Err
- Ok can contain any type (represented by type parameter T), and Err can contain any type (represented by type parameter E)

## Handling a result

Let's say you have a function as follows:

```rust
fn fetch_record() -> Result<DatabaseRecord, Error> {
    // Some fallible code here
}
```

There are several ways to handle the result returned by `fetch_record`.

### `unwrap` and `expect`
The laziest way is just to call `unwrap()` which returns the value contained in the Ok branch of the enum or panics (killing the program) if the result is an Err variant.

```rust
let my_record = fetch_record().unwrap();
```

There is another variant, `expect`, which at lets you at least provide a message when the program dies:
```rust
let my_record = fetch_record().expect("Failed to retrieve record from the database!");
```

This is not very practical, since often times you don't want your program to die after an error. Another way would be to use pattern matching.

### Pattern matching
```rust
match fetch_record() => {
    Ok(record) => {
        // {:?} says to format the value using the 'Debug' format.
        println!("My record is {:?}", record); 
    },
    Err(err) => {
        println!("Error while retrieving record {:?}", err);
    }
}
```

This is very idiomatic in rust and in functional programming in general. You can also use this to match different error variants and react differently. This varies based on how the library's error type is set up. Here's a made up example:

```rust
enum MyError {
    TimedOutError(ErrorContext),
    NotFoundError,
    IoError,
    UnknownError
}

match fetch_record() => {
    Ok(record) => {
        println!("My record is {:?}", record);
    },
    Err(MyError::NotFoundError) => {
        println!("Record not found!";
    },
    Err(MyError::TimedOutError(context)) => {
        println!("Lookup timed out! {:?}", context);
    },
    _ => {
        println!("Some other error");
    }
}
```

The problem is that it gets verbose if there are a lot of calls that could error, especially since the rest of your code that uses the record will have to be indented in the `Ok` branch. This can further be improved through the use of the `?` operator.

### `?` Operator
The `?` operator is applied to a `Result` and returns the inner value if the result is `Ok`, or causes the enclosing function to return the result early if the result is `Err`. Let's see an example:

 Suppose you have the following functions:
```rust
fn get_cat() -> Result<String, Error> {
    // Returns Ok("cat") or an Error
}

fn get_dog() -> Result<String, Error> {
    // Returns Ok("dog") or an Error
}

fn get_rabbit() -> Result<String, Error> {
    // Returns Ok("rabbit") or an Error
}
```

Now I could write my own function which calls these three, and returns a `Result` quite cleanly:

```rust
fn concatenate_values() -> Result<String, Error> {
    let my_cat = get_cat()?; // Will return early if there's an error
    let my_dog = get_dog()?; // Will return early if there's an error
    let my_rabbit = get_rabbit()?; // Will return early if there's an error

    // Return Ok("cat dog rabbit") if all went well
    return Ok(format!("{} {} {}", my_cat, my_dog, my_rabbit)); 
}
```

There are two problems with the above approach: 
1. Not every `Error` will be the same type, so if `get_cat()` and `get_dog()` return different error types, this won't compile
2. You lose context about what you were trying to do in the function when you returned the error, especially in multiple levels of calls.
- As an example, if the caller logs the error, which turns out to be `error: busy playing`, it would not be obvious whether it happened during `get_cat()`, `get_dog()`, or `get_rabbit()`. This matters even more in deep and complicated call stacks.

Enter `anyhow::Error`, a library that fixes both of these problems

### `anyhow::Error`
`anyhow::Error` is a library that fixes the two problems we saw in the previous section. 
- It defines a type, `anyhow::Error` that wraps the generic `Error` trait, so that all errors will work with it (because all errors implement the Error trait)
- It defines a method, `.context()` that is applied to a `Result` and returns another `Result`. If the `Result` is `Ok`, it returns the same object. If it is an `Error`, it attaches a piece of context you specify to the existing contents of the error. It will be nicely displayed when you finally print it.
Let's see an example:

```rust
fn delete_data_from_database(id: i32) -> Result<(), anyhow::Error> {
    connection = db.connect().context("Couldn't connect to database")?;
    connection.prepare("DELETE from table where id = :id")
              .context("Failed to prepare delete statement")?;
    connection.bind(":id", id).context("Failed to bind id to delete statement")?;
    connection.execute().context("Failed to execute delete statement")?;

    return Ok(())
}

fn main () {
    let id = 22;
    match delete_data_from_database(id) {
        Ok => {
            log::info!("Successfully deleted id {}", id);
        },
        Err(err) => {
            log::error!("{:?}", err.context("Couldn't delete data from database")); // Operator report
            println!("Request failed due to internal error. Try again later"); // User feedback
            // Any further actions you want to take here
        }
    }
}

```

Now, let's say the connection to the database fails. Your error would look something like:

```
Couldn't delete data from database:

Caused by:
1. Couldn't connect to database
2. Could not connect to localhost:5432 (Connection refused)
```

Looks great, it's incredibly easy to tell what went wrong now. 

Without the context, we would have just gotten the terse error `Could not connect to localhost:5432 (Connection refused)` which might be hard to tell is a database connection, especially in a complex application which connects to multiple services.

What if the bind failed?:

```
Couldn't delete data from database:

Caused by:
1. Failed to bind id to delete statement
2. expected String, found int
```

This is great, our errors have context at every level of what we were trying to do, from the highest level to the lowest. No need for a debugger anymore! Let's now try to add some retry logic for when we get a timeout.

```rust
fn delete_data_from_database(id: i32) -> Result<(), anyhow::Error> {
    match db.connect() {
        Ok(connection) => {
            connection.prepare("DELETE from table where id = :id")
                      .context("Failed to prepare delete statement")?;
            connection.bind(":id", id).context("Failed to bind id to delete statement")?;
            connection.execute().context("Failed to execute delete statement")?;

            return Ok(())
        },
        Err(TimedOutError) => {
            // Retry the request after some time
            log::warn!("Timed out when getting db connection!");
            thread::sleep(Duration::from_millis(100));
            return delete_from_database(id);
        },
        other_error => {
            // Bubble up other errors with our context                                         
            return other_error.context("Failed to connect to database");
        }
    }
}
```

This is a little more verbose, but still quite clean. We match out the cases we can action on and bubble up the rest with our context. That's it! We now have a system which gives us incredibly helpful errors without a ton of if else statements or having to deal with the weird behavior of exceptions.

A great example of a clear log from my personal project:

```
Could not play file "/home/alex/Downloads/bt.ogg"

Caused by:
    0: Failed to get specs for media source "/home/alex/Downloads/bt.ogg"
    1: Failed to open path "/home/alex/Downloads/bt.ogg"
    2: No such file or directory (os error 2)
```

You can see the clear progression from big picture (couldn't play file) to the actual low level OS error (No such file or directory) with good context in between.


## References

- Thanks to this post https://www.lpalmieri.com/posts/error-handling-rust/ which got me thinking about Rust error handling

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
